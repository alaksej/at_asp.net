﻿google.com:

1. Строка для ввода кейворда для поиска 
	XPATH: //*[@id="gs_htif0"] 
	CSS: #gs_htif0
2. Кнопка "Поиск в Google" 
	XPATH: //input[@name="btnK"] 
	CSS: input[name="btnK]"]
3. Кнопка "Мне повезет" 
	XPATH: //input[@name="btnI"] 
	CSS: input[name="btnI"]

На странице результатов гугла:

1. Ссылки на результаты поиска (все)
	XPATH: //h3[@class="r"]/a 
	CSS: .r > a
2. 5-я буква о в Goooooooooogle (внизу, где пагинация) 
	XPATH: //*[@id='nav']/tbody/tr/td[6] 
	CSS: #nav > tbody td:nth-child(6) .ch

yandex.com:

1. Login input field
	XPATH: //*[contains(@class, 'auth__username')]/*/input
	CSS: .auth__username input
2. Password input field
	XPATH: //*[contains(@class, 'auth__password')]/*/input
	CSS: .auth__password input
3. Enter" button in login form 
	XPATH: //*[contains(@class, 'auth__button')]
	CSS: .auth__button

Страница яндекс-почты:

1. Ссылка "Входящие" 
	XPATH: //*[contains(@class, 'ns-view-folders')]/a[@href="#inbox"]
	CSS: .ns-view-folders a[href="#inbox"]
2. Ссылка "Исходящие" 
	XPATH: //*[contains(@class, 'ns-view-folders')]/a[@href="#sent"]
	CSS: .ns-view-folders a[href="#sent"]
3. Ссылка "Спам" 
	XPATH: //*[contains(@class, 'ns-view-folders')]/a[@href="#spam"]
	CSS: .ns-view-folders a[href="#spam"]
4. Ссылка "Удаленные" 
	XPATH: //*[contains(@class, 'ns-view-folders')]/a[@href="#trash"]
	CSS: .ns-view-folders a[href="#trash"]
5. Ссылка "Черновики" 
	XPATH: //*[contains(@class, 'ns-view-folders')]/a[@href="#draft"]
	CSS: .ns-view-folders a[href="#draft"]
6. Кнопка "Новое письмо" 
	XPATH: //*[contains(@class, 'ns-view-container-desc')]/a[@href="#compose"]
	CSS: .ns-view-container-desc a[href="#compose"]
7. Кнопка "Обновить" 
	XPATH: //*[contains(@class, 'ns-view-container-desc')]/*[@data-click-action="mailbox.check"]
	CSS: .ns-view-container-desc *[data-click-action="mailbox.check"]
8. Кнопка "Отправить" (на странице нового письма) 
	XPATH: //button[@type="submit"]
	CSS: button[type="submit"]
9. Кнопка "Пометить как спам" 
	XPATH: //*[contains(@class, 'ns-view-toolbar-button-spam')]
	CSS: .ns-view-toolbar-button-spam
10. Кнопка "Пометить прочитанным" 
	XPATH: //*[contains(@class, 'ns-view-toolbar-button-mark-as-read')]
	CSS: .ns-view-toolbar-button-mark-as-read
11. Кнопка "Переместить в другую директорию" 
	XPATH: //*[contains(@class, 'ns-view-toolbar-button-folders-actions')]
	CSS: .ns-view-toolbar-button-folders-actions
12. Кнопка "Закрепить письмо" 
	XPATH: //*[contains(@class, 'ns-view-toolbar-button-pin')]
	CSS: .ns-view-toolbar-button-pin
13. Селектор для поиска уникального письма 
	XPATH: //span[@title=' -name-of-mail-']/ancestor::a
	CSS4(?): $a span[title=' -name-of-mail-']


Страница яндекс-диска:

1. Кнопка загрузить файлы 
	XPATH: //*[contains(@class, 'button__attach')]
	CSS: .button__attach
2. Селектор для уникального файла на диске 
	XPATH: //*[@title='Зима.jpg']/ancestor::div[contains(@class, 'ns-view-resource ')]
	CSS: $.ns-view-resource div[title='Зима.jpg']
3. Кнопка скачать файл 
	XPATH: //button[contains(@data-click-action, 'resource.download')]
	CSS: button[data-click-action="resource.download"]
4. Кнопка удалить файл
	XPATH: //button[contains(@data-click-action, 'resource.delete')]
	CSS: button[data-click-action="resource.delete"]
5. Кнопка в корзину
	XPATH: //button[contains(@data-click-action, 'resource.delete')]
	CSS: button[data-click-action="resource.delete"]
6. Кнопка восстановить файл 
	XPATH: //button[contains(@data-click-action, 'resource.restore')]
	CSS: button[data-click-action="resource.restore"]