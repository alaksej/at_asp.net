﻿На странице google.com выписать следующие CSS и XPATH селекторы:
1. Строка для ввода кейворда для поиска
input#lst-ib
#lst-ib

2. Кнопка "Поиск в Google"
input[name='btnK']

3. Кнопка "Мне повезет"
input[name='btnI']

На странице результатов гугла:
1. Ссылки на результаты поиска (все)
.g .r>a

2. 5-я буква о в Goooooooooogle (внизу, где пагинация)
a[aria-label='Page 5'] span

На странице yandex.com:
1. Login input field
input[name='login']

2. Password input field
input[name='passwd']

3. "Enter" button in login form
button.auth__button

На странице яндекс почты (у кого нет ящика - зарегистрируйте)
1. Ссылка "Входящие"
a[data-params="fid=1"]

2. Ссылка "Исходящие"
a[href='#sent']

3. Ссылка "Спам"
a[href='#spam']

4. Ссылка "Удаленные"
a[href='#trash']

5. Ссылка "Черновики"
a[href='#draft']

6. Кнопка "Новое письмо"
a[href='#compose']

7. Кнопка "Обновить"
div[data-click-action='mailbox.check']

8. Кнопка "Отправить" (на странице нового письма)
button#nb-32

9. Кнопка "Пометить как спам"
div.ns-view-toolbar-button-spam

10. Кнопка "Пометить прочитанным"
div.ns-view-toolbar-button-mark-as-read


11. Кнопка "Переместить в другую директорию"
div.ns-view-toolbar-button-folders-actions

12. Кнопка "Закрепить письмо"
div.ns-view-toolbar-button-pin

13. Селектор для поиска уникального письма
a span[title="Как читать почту с мобильного"]

На странице яндекс диска
1. Кнопка загрузить файлы
input.button__attach

2. Селектор для уникального файла на диске
input.input__control

3. Кнопка скачать файл
button#nb-209

4. Кнопка удалить файл
button#nb-210

5. Кнопка в корзину
a[href='/client/trash']

6. Кнопка восстановить файл
button#nb-46