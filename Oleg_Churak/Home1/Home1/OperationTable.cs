﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Home1
{
    public class OperationTable 
    {

        public int Sum(int a, int b)
        {
            return (a + b);
        }

        public int Sub(int a, int b)
        {
            return (a - b);
        }

        public float Div(int a, int b)
        {
            if (b == 0)
                throw new ArgumentException("Divider should be not 0.");
            return (a / b);
        }

        public int Multiply(int a, int b)
        {
            return (a * b);
        }

        public float Average(int a, int b)
        {
            return ((a + b) / 2);
        }

        public float Average(IHandlingList handlingList)
        {
            var list = handlingList.GetListOfInts(5);
            if (list.Count() == 0)
                throw new ArgumentException("List is empty.");

            return (list.Sum() / list.Count());
        }
    }
}
