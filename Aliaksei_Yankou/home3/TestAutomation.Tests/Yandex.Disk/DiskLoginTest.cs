﻿using NUnit.Framework;
using TestAutomation.Services.Models;
using TestAutomation.Services;
using System.Configuration;

namespace TestAutomation.Yandex.Disk.Tests
{
	[TestFixture]
	public class DiskLoginTest : BaseYandexDiskTest
	{
		private readonly string expected_error_message_in_case_of_account_not_exist = 
			ConfigurationManager.AppSettings["AccountNotExistentErrorMessageEn"];
		private readonly string expected_error_message_in_case_of_wrong_password = 
			ConfigurationManager.AppSettings["WrongUserNameOrPasswordErrorMessage"];

		[Test]
		public void DiskLoginTest_LoginWithNonExistingAccount_CorrectErrorMessagePresent()
		{
			DiskLoginService.Login(AccountFactory.NonExistedAccount);
			string actualErrorMessage = DiskLoginService.RetrieveErrorOnFailedLogin();
			Assert.That(expected_error_message_in_case_of_account_not_exist, Is.EqualTo(actualErrorMessage));
		}

		[Test]
		public void DiskLoginTest_LoginWithAccountWithWrongPaswd_CorrectErrorMessagePresent()
		{
			DiskLoginService.Login(AccountFactory.AccountWithWrongPassword);
			string actualErrorMessage = DiskLoginService.RetrieveErrorOnFailedLogin();
			Assert.That(expected_error_message_in_case_of_wrong_password, Is.EqualTo(actualErrorMessage));
		}

		[Test]
		public void DiskLoginTest_LoginWithValidAccount_UserNamePresent()
		{
			DiskLoginService.Login(AccountFactory.ValidAccount);
			string actualUserName = DiskLoginService.RetrieveUserNameOnLoginSuccess();
			string expectedUserName = AccountFactory.ValidAccount.Login;
			Assert.That(expectedUserName, Is.EqualTo(actualUserName));
		}
	}
}
