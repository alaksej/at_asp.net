﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAutomation.Services.Models
{
	public static class AccountFactory
	{
		private static readonly string defaultMailUserLogin = ConfigurationManager.AppSettings["DefaultUserLogin"];
		private static readonly string defaultMailUserPassword = ConfigurationManager.AppSettings["DefaultUserPassword"];
        private static readonly string defaultUserEmail = ConfigurationManager.AppSettings["DefaultUserEmail"];

        private static readonly string WRONG_PASSWORD = defaultMailUserPassword + new Random().Next();
		private static readonly string NONEXISTENT_LOGIN = defaultMailUserLogin + new Random().Next();
		private static readonly string NONEXISTENT_EMAIL = defaultUserEmail + new Random().Next();

		public static UserAccount ValidAccount
		{
			get { return new UserAccount(defaultMailUserLogin, defaultMailUserPassword, defaultUserEmail); }
		}

		public static UserAccount AccountWithWrongPassword
		{
			get { return new UserAccount(defaultMailUserLogin, WRONG_PASSWORD, defaultUserEmail); }
		}

		public static UserAccount NonExistedAccount
		{
			get { return new UserAccount(NONEXISTENT_LOGIN, defaultMailUserPassword, NONEXISTENT_EMAIL); }
		}
	}
}
