﻿using OpenQA.Selenium;
using System.IO;
using System.Threading.Tasks;
using TestAutomation.Framework;
using TestAutomation.Pages;

namespace TestAutomation.Services
{
	public static class DiskService
	{
		public static void UploadFile(FileInfo file)
		{
			DiskMainPage.UploadButton.SendKeys(file.FullName);
			if (DiskMainPage.ContainsReplaceButton())
			{
				DiskMainPage.ReplaceButton.Click();
			}
			// Implicitly wait until upload is complete
			var iconComplete = DiskMainPage.UploadCompleteIcon;
		}

		public static void CloseUploadCompleteDialog()
		{
			DiskMainPage.UploadCompleteDialogCloseButton.Click();
			Task.Delay(500).Wait();
		}

		public static void RefreshPage()
		{
			Browser.Instance.Refresh();
			Task.Delay(1000).Wait();
		}

		public static bool ContainsFile(FileInfo file)
		{
			return DiskMainPage.ContainsFile(file.Name);
		}

		public static bool ContainsItemRemovedNotification()
		{
			return DiskMainPage.ContainsItemRemovedNotification();
		}

		public static void SelectFile(FileInfo file)
		{
			DiskMainPage.GetFileName(file.Name).Click();
			Task.Delay(1000).Wait();
		}

		public static void DeleteSelectedFiles()
		{
			DiskMainPage.DeleteButton.Click();
			Task.Delay(4000).Wait();
		}

		public static void OpenTrash()
		{
			DiskMainPage.Trash.Click();
			Task.Delay(500).Wait();
		}
	}
}
