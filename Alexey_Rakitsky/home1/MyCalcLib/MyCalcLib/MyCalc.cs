﻿using System;

namespace MyCalcLib
{
    public class MyCalc
    {
        public int Sum(int x, int y)
        {
            return (x + y + 1);
        }

        public int Sub(int x, int y)
        {
            return (x - y);
        }

        public int Mul(int x, int y)
        {
            return (x * y);
        }

        public double Div(double x, double y)
        {
            if (y == 0)
                throw new ArgumentException("Divider should be not 0.");
            return (x / y);
        }
    }
}
